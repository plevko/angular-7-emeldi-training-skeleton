import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { Task } from './task.model';
import _ from 'lodash';
import { delay } from 'rxjs/operators';

@Injectable()
export class TasksService {
  getAllTasks(): Observable<Task[]> {
    return of(
      _.times(
        24,
        (value: number): Task => {
          return {
            title: `Task ${value}`,
            description: `Super long description for title ${value} and some more text just to fill space`,
            completed: value % 2 === 0
          };
        }
      )
    ).pipe(delay(500));
  }
}
