import { Component } from '@angular/core';

@Component({
  selector: 'emeldi-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
}
