import { TasksComponent } from './tasks/tasks.component';
import { Routes } from '@angular/router';
import { OpenPositionsComponent } from './open-positions/open-positions.component';

export const appRoutes: Routes = [
  {
    path: 'open-positions',
    component: OpenPositionsComponent
  },
  {
    path: 'tasks',
    component: TasksComponent
  },
  {
    path: '**',
    redirectTo: 'open-positions'
  }
];
