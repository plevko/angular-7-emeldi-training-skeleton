export class OpenPosition {
  title: string;
  detail: string;
  salary: number;
}
