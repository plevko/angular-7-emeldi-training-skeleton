import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import _ from 'lodash';
import { OpenPosition } from './open-position.model';
import { delay } from 'rxjs/operators';

@Injectable()
export class OpenPositionsService {
  getAllOpenPositions(): Observable<OpenPosition[]> {
    return of(
      _.times(
        18,
        (value: number): OpenPosition => {
          return {
            title: `Open position ${value}`,
            detail: `Open position detail for id: ${value}`,
            salary: 100 * value
          };
        }
      )
    ).pipe(delay(1000));
  }
}
